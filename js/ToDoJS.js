/**
 * Created by Rita Dias on 26-01-2015.
 */

function Chore(id, choreName) {
    this.id = id;
    this.choreName = choreName;
}

function AppViewModel() {

    var index = 0; // index of chores
    var self = this;
    var isPlaying = false; // if the easter egg is on/off

    self.chores = ko.observableArray([]);
    self.completeChores = ko.observableArray([]);
    self.diggyLines = [new Chore(index++,"I am a dwarf"),
        new Chore(index++,"and I'm digging a hole"),
        new Chore(index++,"diggy diggy hole"),
        new Chore(index++,"diggy diggy hole")];

    /* Adds new chore to the list of chores */
    self.addNewChore = function() {
        self.chores.push(new Chore(index++,document.getElementById("input").value));
        document.getElementById("input").value = "";
    };

    /* Remove chore from the list of chores */
    self.removeChore = function(chore) {
        self.chores.remove(chore);
        self.completeChores.remove(chore);
    };

    /* Remove complete chores */
    self.removeComplete = function() {
        var conf = confirm("Are you certain?");
        if(conf) {
            self.chores.removeAll(self.completeChores());
            self.completeChores.removeAll();
        }
    };

    /* Returns true if chore is complete, false if not */
    self.setEnabled = function(chore) {
        return self.completeChores().indexOf(chore) == -1
    };

    /* Auxiliar function to determine if it's singular or plural  */
    self.nameSelectTotal = function() {
        if(self.chores().length == 1)
            return "chore";
        else
            return "chores";
    };

    /* Auxiliar function to determine if it's singular or plural  */
    self.nameSelectComplete = function() {
        if(self.completeChores().length == 1)
            return "chore";
        else
            return "chores";
    }

    /* Auxiliar function to determine if it's singular or plural  */
    self.nameSelectCurrent = function(){
        if(self.chores().length - self.completeChores().length == 1)
            return "chore";
        else
            return "chores";
    }

    /* Part of easter egg,
        starts/stops playing when clicking the button
        sets the image or the stop on the element
        adds/removes the spoof chores
     */
    self.diggyHole = function() {
        if(isPlaying) {
            self.chores.removeAll(self.diggyLines)

            stop();
            isPlaying = false;
            document.getElementById('shovel').innerHTML = "<img src='./img/diggyShovel.png'>";
        } else {
            self.diggyLines.forEach(function(i) {
                self.chores.push(i);
            });

            start();
            isPlaying = true;
            document.getElementById('shovel').innerHTML = "Stop";
        }
    }


}

ko.applyBindings(new AppViewModel());

/*
This is part of the easter egg,
    used the youtube API to be able to get the video working properly
*/

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'ytWz0qVvBZ0',
        playerVars: {'start':'68'}
    });
}

function stop() {
    player.stopVideo();
}
function start() {
    player.setVolume(50);
    player.playVideo();
}

